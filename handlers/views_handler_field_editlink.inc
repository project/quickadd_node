<?php

/**
 * Field handler to provide simple renderer that creates a into a clickable edit link.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_editlink extends views_handler_field {
  function query() {
    $this->ensure_my_table();
    // Add the field.

   $this->field_alias = $this->query->add_field('node', 'type');
   // $this->add_additional_fields();
  
  }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['display_as_link'] = array('default' => TRUE);
    return $options;
  }


  /**
   * Provide link to the page being visited.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_as_link'] = array(
      '#title' => t('Display as link'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_as_link']),
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    $value = $values->{$this->field_alias};
    watchdog('Options', print_r($this->options, TRUE));
    if (user_access("edit any {$node->type} content")) {
      return l('Edit',"node/{$values->nid}/edit",array('query'=>drupal_get_destination()));
    }
  }
}
