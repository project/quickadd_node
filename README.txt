
Default node add tab
-------------------
Enable the module by going to admin -> site building -> Modules -> Quicknode-add
Once the module is activated, users with create content type permission will be able to see the new additional tab.

Node links
----------
To add "Edit" "Add <content type> " links to node , 
1) goto admin -> site configuration -> Quickadd node
2) Select the option using checkboxes.
3) Edit and add <content type> links will be displayed as part of node links

Views integration
-----------------
1) Create a new node view 
2) Click on the fields 
3) Select group Quick add
4) Select fields Quickadd-edit links
5) Edit link will be displayed as part of view fields.